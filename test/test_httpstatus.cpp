/*
 * =====================================================================================
 *
 *       Filename:  testurihandler.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08/10/2012 06:44:28 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include "http_status.hpp"

#include <iostream>

int main(int argc, char *argv[])
{
    using namespace tiny_http;
    HttpStatus status;

    status.set(404);
    std::cout << status.getStatusString() << std::endl;

    status.setUserString("Sorry, resource is not found");
    std::cout << status.getStatusString() << std::endl;

    status.appendString("But we'll search it harder");
    std::cout << status.getStatusString() << std::endl;
    //std::cout << status << std::endl;

    status.clear();
    std::cout << status.getStatusString() << std::endl;

    return 0;
}
