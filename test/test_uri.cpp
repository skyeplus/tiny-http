/*
 * =====================================================================================
 *
 *       Filename:  testmime.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08/10/2012 05:54:47 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include "uri.hpp"

#include <iostream>

int main(int argc, char *argv[])
{
    using namespace tiny_http;
    Uri uri;

    uri = "/bla/bla.ext?blabla=blabla#bla";

    std::cout << uri << std::endl;
    std::cout << uri.path() << std::endl;
    std::cout << uri.ext() << std::endl;
    std::cout << uri.query() << std::endl;
    std::cout << uri.fragment() << std::endl;
    std::cout << "authority present: " << uri.isAuthorityPresent() << std::endl;

    uri = "http://host.com:80/bla/bla.ext?blabla=blabla#bla";
    std::cout << uri << std::endl;
    std::cout << uri.path() << std::endl;
    std::cout << uri.ext() << std::endl;
    std::cout << uri.query() << std::endl;
    std::cout << uri.fragment() << std::endl;
    std::cout << "authority present: " << uri.isAuthorityPresent() << std::endl;
}
