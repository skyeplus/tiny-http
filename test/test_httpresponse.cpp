#include "http_response.hpp"

#include <iostream>
#include "uri_handler.hpp"

int main(int argc, char *argv[])
{
    using namespace tiny_http;
    HttpResponse response;
    response["Content-Type"] = "text/plain";
    response.status().set(200);
    response.outputBody(false);

    std::cout << response << std::endl;

    response.outputBody(true);
    std::cout << response << std::endl;

    return 0;
}
