#include "http_header.hpp"

#include <iostream>

int main(int argc, char *argv[])
{
    using namespace tiny_http;
    HttpHeader header;
    header["Content-Type"] = "text/plain";
    header.method() = "GET";
    header.uri() = "1.txt";
    header.httpVersion() = "HTTP/1.0";
    header.body() = "12345678\nbody-end";

    std::cout << header << std::endl;
    return 0;
}
