/*
 * =====================================================================================
 *
 *       Filename:  testmime.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08/10/2012 05:54:47 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include "mime_detector.hpp"
#include "mime_db.hpp"

#include <iostream>

int main(int argc, char *argv[])
{
    using namespace tiny_http;
    std::shared_ptr<MimeDb> mime = std::make_shared<MimeDb>();
    mime->init("/etc/mime.types");
    MimeDetector::init(mime);

    std::cout << "1.txt " << MimeDetector::detect("1.txt") << std::endl;
    std::cout << "1.png " << MimeDetector::detect("1.png") << std::endl;
    std::cout << "1.html " << MimeDetector::detect("1.html") << std::endl;
}
