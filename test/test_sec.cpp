/*
 * =====================================================================================
 *
 *       Filename:  testmime.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08/10/2012 05:54:47 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include "security_checker.hpp"

#include <iostream>

int main(int argc, char *argv[])
{
    using namespace tiny_http;
    std::string path;

    path = "/index.html";
    std::cout << "check: " << path << ", result: " << SecurityChecker::checkUri(path) << std::endl;

    path = "/../index.html";
    std::cout << "check: " << path << ", result: " << SecurityChecker::checkUri(path) << std::endl;

    path = "///index.html";
    std::cout << "check: " << path << ", result: " << SecurityChecker::checkUri(path) << std::endl;

    path = "http://localhost/index.html";
    std::cout << "check: " << path << ", result: " << SecurityChecker::checkUri(path) << std::endl;

    path = "http://localhost/dir/../../index.html";
    std::cout << "check: " << path << ", result: " << SecurityChecker::checkUri(path) << std::endl;

    return 0;
}
