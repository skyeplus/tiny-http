#include "thread_pool.hpp"

#include <iostream>

using namespace tiny_http;

class TestRunnable: public Runnable
{
    int index;
public:
    TestRunnable(int i) : index(i) {}
    void *run() {
        std::cout << "Runnable test (" << index << ")" <<std::endl;
        sleep(1);
        std::cout << "exiting (" << index <<")" << std::endl;
        return NULL;
    }
};

int main(int argc, char *argv[])
{
    ThreadPool pool(10);
    std::cout << "Pool size: " << pool.getPoolSize() << std::endl;
    for (int i = 0; i < 30; i++)
    {
        Thread *thread = pool.getThread();
        if (thread)
            thread->start(std::make_shared<TestRunnable>(i));
    }
    
    sleep(4);
    std::cout << "Pool size: " << pool.getPoolSize() << std::endl;
}
