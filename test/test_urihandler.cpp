/*
 * =====================================================================================
 *
 *       Filename:  testurihandler.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08/10/2012 06:44:28 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include "uri_handler.hpp"
#include "file_uri_handler.hpp"

int main(int argc, char *argv[])
{
    using namespace tiny_http;
    std::shared_ptr<UriHandler> handler;
    HttpStatus status;

    UriHandler::registerHandler("txt", std::make_shared<FileUriHandler>(""));
    handler = UriHandler::open("1.txt", status);

    //std::cout << status << std::endl;

    return 0;
}
