/*
 * =====================================================================================
 *
 *       Filename:  testmime.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08/10/2012 05:54:47 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include "socket.hpp"
#include "http_header.hpp"
#include "connection.hpp"

#include <iostream>

int main(int argc, char *argv[])
{
    using namespace tiny_http;
    Socket sock;
    std::cout << "sock opened: " << sock.isOpened() << std::endl;
    bool result;
    result = sock.bind("localhost", 80);
    std::cout << "sock bound: " << result << std::endl;
   
    std::shared_ptr<Connection> conn= sock.waitForConnection();
     
    std::cout << "new connection: " << conn.get() << std::endl;
    #if 0
    char buf[1024];
    memset(buf, 0, sizeof buf);
    int res = conn->read(buf, sizeof buf);
    if (res > 0)
        std::cout << "read '" << std::string(buf, res) << "'" << std::endl;    
    else
        std::cout << "Failed to read" << std::endl;
    #endif

    #if 0
    std::string line;
    do {
        line = conn->readline();
        std::cout << "len: " << line.size() << ", line: " << line << std::endl;
    } while (line.size()); 
    #endif

    HttpHeader header;    
    header.parse(*conn);
    
    std::cout << "Body:\n" << header.body() << std::endl;
}
