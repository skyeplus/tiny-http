/*
 * =====================================================================================
 *
 *       Filename:  testmime.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08/10/2012 05:54:47 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include "file_uri_handler.hpp"
#include <iostream>
#include <fstream>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

int main(int argc, char *argv[])
{
    using namespace tiny_http;
    std::string path = "/home/scout/public_www/index.html";
    std::ifstream f;
    f.open(path);
    std::cout << path << ", opened: " << f.is_open() << std::endl;
    int fd = open(path.c_str(), O_RDONLY);
    if (fd == -1) {
        std::cout << "Error opening " << path << " " << strerror(errno) << std::endl;
    }
    return 0;
}
