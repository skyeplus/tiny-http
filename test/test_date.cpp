/*
 * =====================================================================================
 *
 *       Filename:  date.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08/11/2012 02:07:41 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#include <ctime>
#include <string>
#include <iostream>

int main(int argc, char *argv[])
{
    time_t now;
    std::time(&now);
    struct tm *t = localtime(&now);
    char buf[80];
    size_t len = std::strftime(buf, sizeof buf, "a%, %d %b %Y %X %Z", t);
    std::cout << std::string(buf, len) << std::endl;
    return 0;
}
