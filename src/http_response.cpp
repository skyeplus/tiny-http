#include "http_response.hpp"
#include "logger.hpp"

#include <ctime>

namespace tiny_http {

HttpResponse::HttpResponse() :
    output_body_(true),
    http_ver_("HTTP/1.1"),
    body_(),
    status_(),
    uri_handler_()
{
}

HttpResponse::~HttpResponse()
{
}

HttpStatus &HttpResponse::status()
{
    return status_;
}

std::string &HttpResponse::body()
{
    return body_;
}

std::string &HttpResponse::httpVersion()
{
    return http_ver_;
}

std::string HttpResponse::dateNow()
{
    time_t now;
    std::time(&now);
    struct tm *t = localtime(&now);
    char buf[80];
    size_t len = std::strftime(buf, sizeof buf, "a%, %d %b %Y %X %Z", t);
    std::string currentDate(buf, len);
    (*this)["Date"] = currentDate;

    return currentDate;
}

bool HttpResponse::outputBody()
{
    return output_body_;
}

void HttpResponse::outputBody(bool value)
{
    output_body_ = value;
}

std::shared_ptr<UriHandler> HttpResponse::getUriHandler()
{
    return uri_handler_;
}

void HttpResponse::setUriHandler(std::shared_ptr<UriHandler> handler)
{
    uri_handler_ = handler;
}

std::ostream &operator <<(std::ostream &os, HttpResponse &resp)
{
    std::shared_ptr<UriHandler> handler = resp.getUriHandler();

    if (handler && handler->isDynamic() && handler->isValid()) {
        std::istream &is = handler->getStream();
        while (!is.eof() && os.good()) {
            char buf[1024];
            unsigned int _read;
            _read = is.readsome(buf, sizeof buf);
            os.write(buf, _read);
        }

        return os;
    }

    os << resp.httpVersion() << " " << resp.status().getStatusString();

    HttpResponse::const_iterator it;
    for (it = resp.begin(); it != resp.end(); ++it) {
        os << it->first << ": " << it->second << "\r\n";
    }

    LOGF() << "outputBody: " << resp.outputBody() << std::endl;
    if (resp.outputBody()) {
        LOGF() << "handler " << handler << std::endl;
        if (handler) LOGF() << "handler valid: " << handler->isValid() << std::endl;

        if (handler && handler->isValid()) {
            int len = handler->getContentLength();
            LOGF() << "content length " << len << std::endl;
            if (len > 0) {
                os << "Content-Length: " << len << "\r\n\r\n";

                unsigned int remain = len;
                std::istream &is = handler->getStream();
                while (remain > 0 && !is.eof() && os.good()) {
                    char buf[1024];
                    unsigned int to_read;
                    to_read = remain < sizeof buf ? remain : sizeof buf;
                    is.read(buf, to_read);
                    os.write(buf, to_read);
                }
                is.seekg(0, std::ios::beg);
                LOGF() << "is.eof " << is.eof() << ", os.good " << os.good() << std::endl;
            }
        } else if (!resp.body().empty()) {
            os << "Content-Length: " << resp.body().length() << "\r\n\r\n";
            os << resp.body();
        } else {
            os << "\r\n";
        }
    } else {
        os << "\r\n";
    }

    return os;
}

} // namespace tiny_http

