#include "config.hpp"
#include "logger.hpp"
#include "socket.hpp"
#include "mime_db.hpp"
#include "http_server.hpp"

#include <iostream>

 
int main(int argc, char *argv[])
{
    using namespace tiny_http;
	Config::inst().init(argc, argv);
    Logger::init(Config::inst());

    std::shared_ptr<MimeDb> mime_db = std::make_shared<MimeDb>();
    mime_db->init(Config::inst()["mime-db"]);
    MimeDetector::init(mime_db);

    HttpServer server;

    server.start();
    LOGF() << "Exiting" << std::endl;

	return 0;
}
