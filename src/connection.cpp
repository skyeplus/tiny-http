#include "connection.hpp"
#include "socket.hpp"
#include "logger.hpp"

#include <errno.h>
#include <string.h>
#include <unistd.h>

namespace tiny_http {

Connection::Connection(Socket *sock, int fd) :
    cfd_(fd),
    sock_(sock),
    mutex_(),
    buf_offset_(0),
    buf_()
{
    pthread_mutex_init(&mutex_, NULL);
    buf_ = new char[CONNECTION_BUF_SIZE];
}

Connection::~Connection()
{
    close();
    delete [] buf_;
    pthread_mutex_destroy(&mutex_);
}

void Connection::close()
{
    if (cfd_ != -1)
        ::close(cfd_);
    cfd_ = -1;
}

bool Connection::isOpened()
{
    return cfd_ != -1;
}

int Connection::read_(char *buf, unsigned int size)
{
    //LOGF() << std::endl;
    unsigned int total_read = 0;
    int cur_read;
    int attempts = IO_ATTEMPTS_NUM;
    #if 0
    fd_set rd_s;
    int fd_max = cfd_ + 1;
    FD_ZERO(&rd_s);
    FD_SET(cfd_, &rd_s);

    struct timeval tv;
    memset(&tv, 0, sizeof tv);
    tv.tv_sec = 10;
    tv.tv_usec = 0;
    
    int err;
    err = select(fd_max, &rd_s, NULL, NULL, &tv);
    if (err < 0) {
        LOGF() << "select error " << strerror(errno) << std::endl;
    }
    if (!FD_ISSET(cfd_, &rd_s)) {
        return 0;
    }
    #endif // if 0
    while (attempts > 0 && total_read < size) {
        cur_read = recv(cfd_, buf + total_read, size - total_read, MSG_DONTWAIT);
        if (cur_read == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                attempts--;
                continue;
            } else {
                int save_errno = errno;
                LOGF() << "Error reading socket: " << strerror(errno) << std::endl;
                errno = save_errno;
                return -1;
            }
        }
        if (cur_read == 0) attempts--;
        total_read += cur_read;
    }

    return total_read;
}

int Connection::write(const char *buf, unsigned int size)
{
    pthread_mutex_lock(&mutex_);

    unsigned int total_written = 0;
    int cur_written;
    int attempts = IO_ATTEMPTS_NUM;
    
    while (attempts > 0 && total_written < size) {
        cur_written = send(cfd_, buf + total_written, size - total_written, 0);
        if (cur_written == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                attempts--;
                continue;
            } else {
                int save_errno = errno;
                LOGF() << "Error reading socket: " << strerror(errno) << std::endl;
                errno = save_errno;
                pthread_mutex_unlock(&mutex_);
                return -1;
            }
        }
        if (cur_written == 0) attempts--;
        total_written += cur_written;
    }
    
    pthread_mutex_unlock(&mutex_);

    return total_written;
}

int Connection::read(char *buf, unsigned int size)
{
    pthread_mutex_lock(&mutex_);

    int copied = copyFromInternalBuf(buf, size);
    int total_read = read_(buf + copied, size - copied);

    if (total_read == -1) {
        pthread_mutex_unlock(&mutex_);
        return copied;
    }

    pthread_mutex_unlock(&mutex_);

    return copied + total_read;
}

std::string Connection::readline()
{
    //LOGF() << std::endl;
    pthread_mutex_lock(&mutex_);

    std::string line;

    readToInternalBuf();
    char * newline = (char*)memchr(buf_, '\n', buf_offset_);
    //LOGF() << "newline found (int) " << (long)(newline) << std::endl;
    // We assign even if no newline found. In this case all internal buffer is assigned
    if (newline == NULL) {
        newline = buf_ + buf_offset_;
    } else {
        newline++;
        if ((newline - buf_) >= CONNECTION_BUF_SIZE)
            newline = buf_ + CONNECTION_BUF_SIZE - 1;
        //LOGF() << "to copy " << newline - buf_ << std::endl;
    }

    line.assign(buf_, newline);
    //LOGF() << "readline result: " << line << std::endl;
    relocateToStart(newline - buf_);

    pthread_mutex_unlock(&mutex_);

    return line;
}

int Connection::copyFromInternalBuf(char *buf, unsigned int size)
{
    //LOGF() << "size " << size << std::endl;
    int to_copy = size < buf_offset_ ? size : buf_offset_;
    memcpy(buf, buf_, to_copy);
    relocateToStart(to_copy);

    return to_copy;
}

void Connection::relocateToStart(unsigned int offset)
{
    //LOGF() << "offset " << offset << std::endl;
    if (offset > buf_offset_) {
        LOGF() << "ERROR: offset > buf_offset_" << std::endl;
        return;
    }
    int size = buf_offset_ - offset;
    memmove(buf_, buf_ + offset, size);
    buf_offset_ -= offset;
}

int Connection::readToInternalBuf()
{
    //LOGF() << std::endl;
    int total_read = read_(buf_ + buf_offset_, CONNECTION_BUF_SIZE - buf_offset_);
    if (total_read == -1)
        return buf_offset_;

    buf_offset_ += total_read;
    //LOGF() << "in buf_: " << std::string(buf_, buf_offset_) << std::endl;

    return buf_offset_;
}

} // namespace tiny_http

