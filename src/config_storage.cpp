#include "config_storage.hpp"

#include <sstream>

namespace tiny_http {

bool ConfigStorage::hasKey(const std::string &key) const
{
	return (*this).find(key) != (*this).end();
}

} // namespace tiny_http

