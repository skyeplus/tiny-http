#include "uri.hpp"

namespace tiny_http {

Uri::Uri() :
    parsed_(false),
    path_(),
    query_(),
    fragment_(),
    ext_(),
    authority_present_(false)
{
}

Uri::Uri(const char *uri) :
    std::string(uri),
    parsed_(false),
    path_(),
    query_(),
    fragment_(),
    ext_(),
    authority_present_(false)
{
}

Uri::Uri(const std::string &uri) :
    std::string(uri),
    parsed_(false),
    path_(),
    query_(),
    fragment_(),
    ext_(),
    authority_present_(false)
{
    parse();
}

void Uri::parse()
{
    parsed_ = true;

    if (find("://") != std::string::npos)
        authority_present_ = true;

    size_t qmark_pos, hash_pos, dot_pos;
    qmark_pos = find_first_of('?');
    hash_pos = find_first_of('#', qmark_pos);

    if (qmark_pos != std::string::npos) {
        path_.assign(begin(), begin() + qmark_pos);
        if (hash_pos != std::string::npos) {
            query_.assign(begin() + qmark_pos + 1, begin() + hash_pos);
        } else {
            query_.assign(begin() + qmark_pos + 1, end());
        }
    } else {
        query_.clear();
        path_.assign(*this);
    }

    if (hash_pos != std::string::npos) {
        fragment_.assign(begin() + hash_pos + 1, end());
    } else {
        fragment_.clear();
    }

    dot_pos = path_.find_last_of('.');
    if (dot_pos != std::string::npos) {
        ext_.assign(path_.begin() + dot_pos + 1, path_.end());
    } else {
        ext_.clear();
    }
}

std::string Uri::path()
{
    if (!parsed_)
        parse();

    return path_;
}

std::string Uri::query()
{
    if (!parsed_)
        parse();

    return query_;
}

std::string Uri::fragment()
{
    if (!parsed_)
        parse();

    return fragment_;
}

std::string Uri::ext()
{
    if (!parsed_)
        parse();

    return ext_;
}

bool Uri::isAuthorityPresent()
{
    if (!parsed_)
        parse();

    return authority_present_;
}

} // namespace tiny_http

