#include "config.hpp"
#include "connection_handler.hpp"
#include "http_header.hpp"
#include "http_response.hpp"
#include "logger.hpp"

#include <string>
#include <sstream>

namespace tiny_http {

ConnectionHandler::ConnectionHandler(std::shared_ptr<Connection> conn) :
    conn_(conn)
{
}

ConnectionHandler::~ConnectionHandler()
{
}

void *ConnectionHandler::run()
{
    bool connection_close = false;
    do {
        HttpHeader header;
        HttpResponse response;
        if(!header.parse(*conn_)) {
            continue;
        }
        LOGF() << "new header received:\n" << header << std::endl;

        // Default page
        if (header.uri() == "/") {
            header.uri() = Config::inst()["default-uri"];
        }

        if (header.method() == "GET" || header.method() == "HEAD") {
            std::shared_ptr<UriHandler> handler = UriHandler::open(header.uri(), response.status());
            response.setUriHandler(handler);
            if (header.method() == "HEAD")
                response.outputBody(false);

            if (handler && handler->isValid()) {
                response["Content-Type"] = handler->getContentType();
            } else {
                response["Content-Type"] = "text/html";
                response.body() = "<html><body><p/>" + response.status().getStatusString()+"</body></html>";
                connection_close = true;
            }
        } else if (header.method() == "POST") {
            response.status().set(501);
            response["Content-Type"] = "text/html";
            response.body() = "<html><body><p/> Method not supported </body></html>\r\n";
            connection_close = true;
        } else {
            response.status().set(501);
            response["Content-Type"] = "text/html";
            response.body() = "<html><body><p/> Method not supported </body></html>\r\n";
            connection_close = true;
        }

        response.dateNow();

        std::stringstream ss;
        ss << response;
        LOGF() << std::endl;
        std::string result = ss.str();
        int remain, len = result.length();
        remain = len;

        while (remain > 0) {
            LOGF() << "remain " << remain << std::endl; 
            int written;
            written = conn_->write(result.c_str() + len - remain, remain);
            if (written < 0)
                break;
            remain -= written;
        }

        if (header.find("Connection") != header.end()) {
            if (header["Connection"] == "close") {
                connection_close = true;
                LOGF() << "Connection: close received" << std::endl;
            }
        }
        if (header.httpVersion() == "HTTP/1.0") {
            LOGF() << "HTTP/1.0. Closing connection after session" << std::endl;
            connection_close = true;
        }

        LOGF() << "connection close: " << connection_close << std::endl;
        // FIXME
        //connection_close = true;
    } while (!connection_close);

    conn_->close();

    return NULL;
}

} // namespace tiny_http

