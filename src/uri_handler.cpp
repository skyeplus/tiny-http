#include "http_status.hpp"
#include "logger.hpp"
#include "security_checker.hpp"
#include "uri_handler.hpp"
#include "file_uri_handler.hpp"

#include <fstream>

namespace tiny_http {

UriHandler::UriHandler(const std::string &uri) :
    uri_(uri)
{
}

UriHandler::~UriHandler()
{
}

std::shared_ptr<UriHandler> UriHandler::create(const std::string &uri)
{
    return std::make_shared<UriHandler>(uri);
}

bool UriHandler::isDynamic()
{
    return false;
}

bool UriHandler::isValid()
{
    return false;
}

std::string UriHandler::getContentType()
{
    return "application/octet-stream";
}

unsigned int UriHandler::getContentLength()
{
    return 0;
}

std::istream &UriHandler::getStream()
{
    static std::ifstream st;
    return st;
}

std::map<std::string, std::shared_ptr<UriHandler>> UriHandler::handlers_;

void UriHandler::registerHandler(const std::string &ext, std::shared_ptr<UriHandler> handler)
{
    UriHandler::handlers_[ext] = handler;
}

std::shared_ptr<UriHandler> UriHandler::open(const std::string &uri, HttpStatus &http_status)
{
    std::shared_ptr<UriHandler> handler;

    if (SecurityChecker::checkUri(uri)) {
        http_status.set(403);
        return handler;
    }
    
    Uri uri_(uri);
    std::string ext = uri_.ext();
    if (!ext.empty()) {
        LOGF() << "Extension: " << ext << std::endl;
        
        if (handlers_.find(ext) != handlers_.end()) {
            // cgi handlers go here
            handler = handlers_[ext]->create(uri);
        } else {
            // Default handler
            handler = std::make_shared<FileUriHandler>(uri);
        }
    }

    if (!handler || (handler && !handler->isValid())) {
        LOGF() << "handler.get() " << handler.get() << std::endl;
        if (handler) LOGF() << "handler->isValid() " << handler->isValid() << std::endl;
        http_status.set(404);
        LOGF() << "Failed to create UriHandler for URI " << uri << std::endl;
    } else {
        http_status.set(200);
    }

    return handler;
}

} // namespace tiny_http

