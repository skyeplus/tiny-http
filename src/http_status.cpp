#include "http_status.hpp"

#include <map>
#include <sstream>

namespace tiny_http {

HttpStatus::HttpStatus() :
    status_(),
    user_string_(),
    append_string_()
{
}

HttpStatus::~HttpStatus()
{
}

void HttpStatus::set(const int status)
{
    status_ = status;
}

int HttpStatus::get()
{
    return status_;
}

void HttpStatus::setUserString(const std::string &user)
{
    user_string_ = user;
}

void HttpStatus::appendString(const std::string &append)
{
    append_string_ = append;
}

std::string HttpStatus::getStatusString()
{
    std::ostringstream ss;
    ss << status_ << " ";

    if (!user_string_.empty()) {
        ss << user_string_;
    } else {
        ss << defaultReason(status_);
    }

    if (!append_string_.empty()) {
        ss << " - " << append_string_;
    }

    ss << "\r\n";

    return std::string(ss.str());
}

void HttpStatus::clear()
{
    status_ = 200;
    user_string_.clear();
    append_string_.clear();
}

class DefaultReasonNames: public std::map<int, std::string>
{
public:
    DefaultReasonNames()
    {
        (*this)[101] = "Switching Protocols";
        (*this)[200] = "OK";
        (*this)[201] = "Created";
        (*this)[202] = "Accepted";
        (*this)[203] = "Non-Authoritative Information";
        (*this)[204] = "No Content";
        (*this)[205] = "Reset Content";
        (*this)[206] = "Partial Content";
        (*this)[300] = "Multiple Choices";
        (*this)[301] = "Moved Permanently";
        (*this)[302] = "Found";
        (*this)[303] = "See Other";
        (*this)[304] = "Not Modified";
        (*this)[305] = "Use Proxy";
        (*this)[307] = "Temporary Redirect";
        (*this)[400] = "Bad Request";
        (*this)[401] = "Unauthorized";
        (*this)[402] = "Payment Required";
        (*this)[403] = "Forbidden";
        (*this)[404] = "Not Found";
        (*this)[405] = "Method Not Allowed";
        (*this)[406] = "Not Acceptable";

        (*this)[407] = "Proxy Authentication Required";
        (*this)[408] = "Request Time-out";
        (*this)[409] = "Conflict";
        (*this)[410] = "Gone";
        (*this)[411] = "Length Required";
        (*this)[412] = "Precondition Failed";
        (*this)[413] = "Request Entity Too Large";
        (*this)[414] = "Request-URI Too Large";
        (*this)[415] = "Unsupported Media Type";
        (*this)[416] = "Requested range not satisfiable";
        (*this)[417] = "Expectation Failed";
        (*this)[500] = "Internal Server Error";
        (*this)[501] = "Not Implemented";
        (*this)[502] = "Bad Gateway";
        (*this)[503] = "Service Unavailable";
        (*this)[504] = "Gateway Time-out";
        (*this)[505] = "HTTP Version not supported";
    }
};

std::string HttpStatus::defaultReason(const int status)
{
    static DefaultReasonNames names;
    std::string result;

    if (names.find(status) != names.end()) {
        result = names[status];
    }

    return result;
}

} // namespace tiny_http

