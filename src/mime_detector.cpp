#include "mime_detector.hpp"
#include "mime_db.hpp"
#include "uri.hpp"
#include "logger.hpp"

namespace tiny_http {

std::shared_ptr<MimeDetectorImpl> MimeDetector::mime_ = std::make_shared<MimeDb>();

MimeDetectorImpl::MimeDetectorImpl()
{
}

MimeDetectorImpl::~MimeDetectorImpl()
{
}

void MimeDetector::init(std::shared_ptr<MimeDetectorImpl> impl)
{
    mime_ = impl;
}

std::string MimeDetector::detect(const std::string &uri)
{
    if (mime_.get())
       return mime_->detect(uri);

    LOGF() << std::endl;

    return "application/octet-stream";
}

} // namespace tiny_http

