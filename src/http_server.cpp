#include "config.hpp"
#include "connection_handler.hpp"
#include "http_server.hpp"
#include "logger.hpp"

namespace tiny_http {

HttpServer::HttpServer() :
    threads_(),
    sock_(),
    stop_(false)
{
}

HttpServer::~HttpServer()
{
    stop_ = true;
}

void HttpServer::start()
{
    std::string host("localhost");
    if (!Config::inst()["host"].empty())
        host = Config::inst()["host"];

    int port = Config::inst().get<int>("port");

    // Open socket and bind it
    if (!sock_.open()) {
        LOGF() << "Failed to open socket" << std::endl;
        return;
    }

    if (!sock_.bind(host.c_str(), port)) {
        LOGF() << "Failed to bind socket" << std::endl;
        return;
    }

    while(!stop_) {
        std::shared_ptr<Connection> conn = sock_.waitForConnection();
        if (conn) {
            std::shared_ptr<Runnable> conn_handler = std::make_shared<ConnectionHandler>(conn);
            Thread *thread = threads_.getThread();
            if (thread) {
                thread->start(conn_handler);
            } else {
                LOGF() << "Can't find free thread" << std::endl;
            }
        }
    }
}

void HttpServer::stop()
{
    stop_ = true;
}

} // namespace tiny_http

