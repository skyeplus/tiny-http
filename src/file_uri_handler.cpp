#include "config.hpp"
#include "file_uri_handler.hpp"
#include "mime_detector.hpp"
#include "logger.hpp"

namespace tiny_http {

FileUriHandler::FileUriHandler(const std::string &uri) :
    UriHandler(uri),
    file_stream_(),
    content_type_(),
    length_()
{
    std::string fname = getFileName(uri_);
    LOGF() << "URI resolved to file: " << fname << std::endl;
    file_stream_.open(fname, std::ios::in | std::ios::binary);
    LOGF() << "stream opened: " << file_stream_.is_open() << std::endl;
    length_ = fileSize();
    LOGF() << "file size: " << length_ << std::endl;
}

FileUriHandler::~FileUriHandler()
{
    file_stream_.close();
}

std::shared_ptr<UriHandler> FileUriHandler::create(const std::string &uri)
{
    return std::make_shared<FileUriHandler>(uri);
}

bool FileUriHandler::isDynamic()
{
    return false;
}

bool FileUriHandler::isValid()
{
    return file_stream_.is_open();
}

std::istream &FileUriHandler::getStream()
{
    return file_stream_;
}

std::string FileUriHandler::getContentType()
{
    if (content_type_.empty()) {
        content_type_ = MimeDetector::detect(uri_);
        if (content_type_.empty()) {
            content_type_ = "application/octet-stream";
        }
    }
    return content_type_;
}

unsigned int FileUriHandler::getContentLength()
{
    return length_;
}

std::string FileUriHandler::getFileName(Uri &uri)
{
    std::string base_dir = Config::inst()["public-www"];
    return base_dir + uri.path();
}

unsigned int FileUriHandler::fileSize()
{
    std::streampos fsize;

    fsize = file_stream_.tellg();
    file_stream_.seekg(0, std::ios::end);
    fsize = file_stream_.tellg() - fsize;

    file_stream_.seekg(0, std::ios::beg);

    return fsize;
}

} // namespace tiny_http

