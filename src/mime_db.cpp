#include "mime_db.hpp"
#include "logger.hpp"
#include "uri.hpp"

#include <cctype>
#include <sstream>
#include <fstream>
#include <iostream>
#include <string>

namespace tiny_http {

MimeDb::MimeDb() :
    types_()
{
}

MimeDb::~MimeDb()
{
}

void MimeDb::init(const std::string &file)
{
    parse(file);
}

std::string MimeDb::detect(const std::string &uri)
{
    Uri uri_(uri);
    std::string ext = uri_.ext();

    if (!ext.empty() && types_.find(ext) != types_.end()) {
        return types_[ext];
    }

    return "application/octet-stream";
}

void MimeDb::parse(const std::string &file)
{
    std::ifstream f(file);
    if (!f.is_open()) {
        // try to use local copy before giving up.
        f.open("mime.types");
    }

    if (f.is_open()) {
        std::string line;
        size_t pos, len;
        std::string type;
        std::string ext;

        while (!f.eof()) {
            std::getline(f, line);
            if (!line.empty()) {
                len = line.length();
                pos = 0;
                while (pos < len && std::isspace(line[pos])) pos++;

                if (pos < len && line[pos] == '#')
                    continue;

                std::istringstream ss(line);
                if (!(ss >> type)) {
                    LOGF() << "Failed to parse type from string " << line << std::endl;
                    continue;
                }

                //std::cout << "type: " << type << ", extentions: ";

                while (ss >> ext) {
                    //std::cout << " " << ext;
                    if (!ext.empty() && ext[0] != '#')
                        types_[ext] = type;
                }
                //std::cout << std::endl;
            }
        }

    } else {
        LOGF() << "Failed to open mime.types file: " << file << std::endl;
    }
}

} // namespace tiny_http

