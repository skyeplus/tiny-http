#include "security_checker.hpp"
#include "uri.hpp"
#include "logger.hpp"

namespace tiny_http {

int SecurityChecker::checkUri(const std::string &uri)
{
    int depth = -1; // path depth adjusted for first slash
    Uri uri_(uri);
    std::string path = uri_.path();
    size_t pos = 0, len = path.length();
    enum { SEARCHING_SLASH, SLASH_FOUND, DOT_FOUND, SECOND_DOT_FOUND } state = SEARCHING_SLASH;

    //LOGF() << std::endl;
    if (uri_.isAuthorityPresent()) {
        // skip two slashes
        int count = 2;
        while (count > 0)
            if (path[pos++] == '/')
                count--;
    }
    //LOGF() << "uri_.isAuthorityPresent() " << uri_.isAuthorityPresent() <<
    //        " skipped " << pos << std::endl;

    for (; pos < len; ++pos) {
        switch (state) {
        case SEARCHING_SLASH:
            if (path[pos] == '/') {
                state = SLASH_FOUND;
                depth++;
            }
            break;
        case SLASH_FOUND:
            if (path[pos] == '.') {
                state = DOT_FOUND;
            } else if (path[pos] == '/') {
                // another slash - skipping
            } else {
                state = SEARCHING_SLASH;
            }
            break;
        case DOT_FOUND:
            if (path[pos] == '.') {
                state = SECOND_DOT_FOUND;
            } else if (path[pos] == '/') {
                state = SLASH_FOUND;
            } else {
                state = SEARCHING_SLASH;
            }
            break;
        case SECOND_DOT_FOUND:
            if (path[pos] == '/') {
                depth--;
                state = SLASH_FOUND;
            } else {
                state = SEARCHING_SLASH;
            }
            break;
        }
    }

    //LOGF() << "depth: " << depth << std::endl;
    if (depth < 0) {
        LOGF() << "Attempt to leave server's public directory detected" << std::endl;
        return -1;
    }

    return 0;
}

} // namespace tiny_http

