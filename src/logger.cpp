#include "logger.hpp"

#include <ctime>

namespace tiny_http {

LoggerImpl::LoggerImpl()
{}

std::ostream &LoggerImpl::getStream()
{
	return std::clog;
}

void Logger::init(Config &cfg)
{
	if (Logger::_logger)
		delete Logger::_logger;

	// parse config and choose appropriate logger implementation
	// Using default
	Logger::_logger = new LoggerImpl();
}

std::ostream &Logger::log()
{
	if (Logger::_logger)
	{
		return Logger::_logger->getStream();
	}

	return std::clog;
}

std::ostream &Logger::log(const char *file, const char *function, int line)
{
	std::ostream *os = &std::clog;
    time_t current_time;
    struct tm * timeinfo;
    char buffer [80];

    time(&current_time);
    timeinfo = localtime(&current_time);
    strftime(buffer, sizeof buffer, "%x %X",timeinfo);

	if (Logger::_logger)
	{
		os = &Logger::_logger->getStream();
	}
	(*os) << buffer << " " << file << ", " << function << ":" << line << ": ";
    (*os).flush();

	return *os;
}

Logger::Logger()
{
}

Logger::~Logger()
{
	// Since Logger is singleton it will be destroyed during global cleanup
	// We can safely delete _logger without necessity to track number of references
	delete Logger::_logger;
	Logger::_logger = NULL;
}

LoggerImpl *Logger::_logger = NULL;

} // namespace tiny_http

