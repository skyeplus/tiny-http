#include "http_header.hpp"
#include "logger.hpp"

#include <cctype>

namespace tiny_http {

HttpHeader::HttpHeader() :
    method_(),
    uri_(),
    http_ver_(),
    body_()
{
}

HttpHeader::~HttpHeader()
{
}

static size_t skipWhiteSpace(const std::string &str, const size_t pos)
{
    size_t skip = pos;
    size_t len = str.length();
    while (skip < len && std::isspace(str[skip])) skip++;
    return skip;
}

size_t trimNewLine(std::string &str)
{
    size_t pos;
    pos = str.length() - 1;
    while (str[pos] == '\r' || str[pos] == '\n') pos--;
    str.resize(pos + 1);

    return pos + 1;
}

static bool isEmptyLine(const std::string &str)
{
    return str.empty() || str.find_first_not_of("\r\n", 0) == std::string::npos;
}

std::string &HttpHeader::method()
{
    return method_;
}

std::string &HttpHeader::uri()
{
    return uri_;
}

std::string &HttpHeader::httpVersion()
{
    return http_ver_;
}

std::string &HttpHeader::body()
{
    return body_;
}

bool HttpHeader::parse(Connection &conn)
{
    std::string body;
    std::string line;
    bool result = true;

    //LOGF() << std::endl;
    // Parse first line
    line = conn.readline();
    if (!parseInitialLine(line)) {
        //LOGF() << "Failed to parse initial line: " << line << std::endl;
        return false;
    }
    
    size_t pos;
    std::string field;

    for(;;) {
        line = conn.readline();

        if (isEmptyLine(line)) {
            if(!parseField(field)) {
                LOGF() << "Failed to parse field: " << field << std::endl;
                result = false;
            }
            break;
        }

        pos = skipWhiteSpace(line, 0);
        if (pos > 0) {
            field.append(line.begin() + pos, line.end());
        } else {
            // parse previous field
            if (!parseField(field)) {
                LOGF() << "Failed to parse field: " << field << std::endl;
                result = false;
            }

            // next field
            field = line;
        }
        
        // strip ending CRLF
        trimNewLine(field); 
    }

    // Read body
    if (this->find("Content-Length") != this->end()) {
        int content_length = 0;
        std::istringstream ss((*this)["Content-Length"]);
        

        if ((ss >> content_length) && content_length > 0) {
            unsigned int remains = content_length;
            int to_read, current;
            char buf[1024];

            while (remains > 0) {
                to_read = remains < sizeof buf ? remains : sizeof buf;
                current = conn.read(buf, to_read);
                if (current == -1) {
                    LOGF() << "Failed to read data" << std::endl;
                    result = false; 
                    break;
                }
                body_.append(buf, current);
                remains -= current;
            }
        }
    }

    return result;
}

bool HttpHeader::parseInitialLine(const std::string &str)
{ 
    size_t pos = 0, marker = 0;
    size_t len = str.length();

    while (pos < len && std::isspace(str[pos])) pos++;
    marker = pos;
    
    // Method
    while (pos < len && !std::isspace(str[pos])) pos++;
    method_.assign(str, marker, pos - marker);

    while (pos < len && std::isspace(str[pos])) pos++;
    marker = pos;
    // URI
    while (pos < len && !std::isspace(str[pos])) pos++;
    uri_.assign(str, marker, pos - marker);
    
    while (pos < len && std::isspace(str[pos])) pos++;
    marker = pos;
    // HTTP version
    while (pos < len && !std::isspace(str[pos])) pos++;
    http_ver_.assign(str, marker, pos - marker);

    //std::cout << "method: " << method_ << ", URI: " << uri_ << ", http_ver: " << http_ver_ << std::endl;

    return !method_.empty() && !uri_.empty() && !http_ver_.empty();
}

bool HttpHeader::parseField(const std::string &str)
{
    size_t delim;

    if (str.empty())
        return true;

    delim = str.find_first_of(':', 0);
    if (delim == std::string::npos) {
        LOGF() << "Failed to parse field" << std::endl;
        return false;
    }

    std::string key(str.c_str(), delim);
    std::string value(str.begin() + delim + 1, str.end());

    // strip white space and ending new lines
    value.assign(value.begin() + skipWhiteSpace(value, 0), value.end());
    trimNewLine(value);
        
    std::cout << "Parsed key: " << key << ", value: " << value << std::endl;

    (*this)[key] = value;

    return true;
}

std::ostream &operator <<(std::ostream &os, HttpHeader &header)
{
    os << header.method() << " " << header.uri() << " " << header.httpVersion() << "\r\n";
    HttpHeader::const_iterator it;
    for (it = header.begin(); it != header.end(); ++it) {
        os << it->first << ": " << it->second << "\r\n";
    }
    os << "\r\n";
    os << header.body();

    return os;
}

} // namespace tiny_http

