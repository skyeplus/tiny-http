#include "thread_pool.hpp"
#include "logger.hpp"

namespace tiny_http {

ThreadPool::ThreadPool(unsigned int poolSize) :
    threads_()
{
    threads_.reserve(poolSize);
    for (unsigned int i = 0; i < poolSize; ++i) {
        try {
            Thread *thread = new Thread;
            threads_.push_back(thread);
        } catch (std::bad_alloc &e) {
            LOGF() << "Failed to create new thread" << std::endl;
            break;
        }
    }
}

ThreadPool::~ThreadPool()
{
    std::vector<Thread*>::iterator it;
    for (it = threads_.begin(); it != threads_.end(); ++it) {
        delete *it; // Destructor will automatically stop thread
    }
    threads_.clear();
}

int ThreadPool::getPoolSize() const
{
    return threads_.size();
}

Thread *ThreadPool::getThread()
{
    std::vector<Thread*>::iterator it;
    for (it = threads_.begin(); it != threads_.end(); ++it)
        if (*it && (*it)->getNumberOfTasks() == 0)
            return *it;

    Thread *thread = NULL;
    try {
        thread = new Thread;
        threads_.push_back(thread);
    } catch (std::bad_alloc &e) {
        LOGF() << "Failed to create ne thread" << std::endl;
    }

    return thread;
}

} // namespace tiny_http

