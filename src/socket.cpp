#include "socket.hpp"
#include "logger.hpp"

#include <netdb.h>
#include <string.h>
#include <errno.h>

namespace tiny_http {

static bool get_ip_addr(const char *hostname, const int port, struct sockaddr_in *ipa)
{
    if (!ipa)
        return false;

    ipa->sin_family = AF_INET;
    ipa->sin_port = htons(port);
 
    struct hostent* host = gethostbyname(hostname);
    if(!host){
        LOGF() << "Error resolving hostname " << hostname << std::endl;
 
        return false;
    }
 
    char* addr = host->h_addr_list[0];
    memcpy(&ipa->sin_addr.s_addr, addr, sizeof addr);

    return true;
}

Socket::Socket() :
    sfd_(-1),
    addr_()
{
    open();
}

Socket::~Socket()
{
    close();
}

bool Socket::open()
{
    struct protoent* tcp;
    tcp = getprotobyname("tcp");
 
    sfd_ = socket(PF_INET, SOCK_STREAM, tcp->p_proto);
    if(sfd_ == -1) {
        LOGF() << "Error createing a tcp socket" << std::endl;
 
        return false;
    }

    return true;
}

void Socket::close()
{
    if (sfd_ != -1) {
        ::close(sfd_);
        sfd_ = -1;
    }
}

bool Socket::isOpened()
{
    return sfd_ != -1;
}

bool Socket::bind(const std::string &host, const int port)
{
    bool result  = false;

    LOGF() << std::endl;
    if (get_ip_addr(host.c_str(), port, &addr_))
    {
        if(::bind(sfd_, (struct sockaddr*)&addr_, sizeof addr_) == -1) {
            LOGF() << "Error binding to address " << host << ":" << port <<
                    ", " << strerror(errno) << std::endl;
            return false;
        }

        if(listen(sfd_, 10) == -1) {
            LOGF() << "Error while listening socket, " << strerror(errno) << std::endl;
            return false;
        }

        result = true;
    }

    return result;
}

std::shared_ptr<Connection> Socket::waitForConnection()
{
    struct sockaddr_in client_name;
    socklen_t client_name_len = sizeof(client_name);
    std::shared_ptr<Connection> conn;
 
    LOGF() << std::endl;
    int cfd = accept(sfd_, (struct sockaddr *)&client_name, &client_name_len);
    if(cfd == -1) {
        if(errno == EAGAIN || errno == EWOULDBLOCK) {
            LOGF() << "EAGAIN received while waiting on accept" << std::endl;
        } else {
            LOGF() << "Failed to accept connection: " << strerror(errno) << std::endl;
        }
        return conn;
    }
    conn = std::make_shared<Connection>(this, cfd);
    LOGF() << "connection: " << conn << std::endl;

    return conn;
}

} // namespace tiny_http

