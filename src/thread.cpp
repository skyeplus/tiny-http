#include "thread.hpp"
#include "logger.hpp"

#include <iostream>

namespace tiny_http {

Thread::Thread() :
    thread_id_(0),
    thread_attr_(),
    is_attr_set_(false),
    thread_state_(NOT_STARTED),
    runnables_(), 
    stop_requested_(false)
{
    pthread_mutex_init(&runn_mutex_, NULL);
    pthread_cond_init(&runn_available_, NULL);
}

Thread::~Thread()
{
    if (thread_state_ == RUNNING) {
        stop();
    }
    if (is_attr_set_)
        pthread_attr_destroy(&thread_attr_);

    pthread_mutex_destroy(&runn_mutex_);
    pthread_cond_destroy(&runn_available_);
}

pthread_t Thread::threadId() const
{
    return thread_id_;
}

Thread::ThreadState Thread::threadState() const
{
    return thread_state_;
}

int Thread::getNumberOfTasks() const
{
    return runnables_.size();
}

bool Thread::isStopRequested() const
{
    return stop_requested_;
}

void *Thread::threadRoutine(void *arg)
{
    Thread *pThis = reinterpret_cast<Thread*>(arg);
    return pThis->run();
}

bool Thread::setAttr(const pthread_attr_t &attr)
{
    if (thread_state_ == NOT_STARTED) {
        thread_attr_ = attr;
        is_attr_set_ = true;
        return true;
    }
    return false;
}

void Thread::startThread()
{
    int res;
    //std::cout << "Starting thread" << std::endl;
    if (thread_state_ == NOT_STARTED) {
        if (!is_attr_set_) {
            pthread_attr_init(&thread_attr_);
            is_attr_set_ = true;
        }
        res = pthread_create(&thread_id_, &thread_attr_, threadRoutine, this);
        if (res < 0) {
            std::cerr << "Failed to start thread" << std::endl;
        } else {
            thread_state_ = RUNNING;
        }
    }
}

void Thread::start(std::shared_ptr<Runnable> runn)
{
    if (!isStopRequested()) {
        if (thread_state_ == NOT_STARTED) {
            startThread();
        }

        pthread_mutex_lock(&runn_mutex_);
        //std::cout << "Task added to work queue" << std::endl;
        runnables_.push_back(runn);
        if (runnables_.size() == 1) {
            pthread_cond_signal(&runn_available_);
        }
        pthread_mutex_unlock(&runn_mutex_);
    }
}

void *Thread::stop()
{
    void *result;
    if (thread_state_ == RUNNING) {
        stop_requested_ = true;
        pthread_cond_signal(&runn_available_);
        pthread_join(thread_id_, &result);
        thread_state_ = NOT_STARTED;
        stop_requested_ = false;
    }
    LOG() << "Thread stopped" << std::endl;
    return result;
}

void * Thread::run()
{
    std::shared_ptr<Runnable> runn;
    for (;;) {
        //std::cout << "Main loop" << std::endl;
        // Get the Task from queue
        pthread_mutex_lock(&runn_mutex_);
        while (runnables_.empty() && !isStopRequested()) {
            pthread_cond_wait(&runn_available_, &runn_mutex_);
        }
        if (isStopRequested() && runnables_.empty()) {
        //cout << "5" << " stop-req: " << isStopRequested() << " empty() " << runnables_.empty() << std::endl;
            pthread_mutex_unlock(&runn_mutex_);
            break;
        }

        runn = runnables_.front();
        runnables_.pop_front();
        pthread_mutex_unlock(&runn_mutex_);

//        result = 
        runn->run();
    }
    return NULL;
}

} // namespace tiny_http

