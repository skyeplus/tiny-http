#include "common.hpp"
#include "config.hpp"

#include <sstream>
#include <iostream>

namespace tiny_http {

Config::Config()
{
}

Config::~Config()
{
}

Config& Config::inst()
{
	static Config cfg;
	return cfg;
}

void Config::init(int argc, char *argv[])
{
	processAll(argc, argv);
}

std::string Config::getUsage() const
{
    return "Usage: tiny_http [OPTIONS]\n"
           "        -port <num>        Specify port number\n"
           "        -config <file>     Use config file\n"
           "        -public_www <path> Use <path> as a root for http server\n"
           "        -mime-db <path>    Specify path to mime.types file\n"
           "                           (default: /etc/mime.types)\n"
           "\n";
}

void Config::setDefaults(ConfigStorage &cfg)
{
	cfg["config-file"] = TINY_HTTP_CONFIG_FILE;
	cfg["port"] = "80";
    cfg["public-www"] = TINY_HTTP_PUBLIC_WWW;
    cfg["mime-db"] = TINY_HTTP_MIME_DB_FILE;
    cfg["default-uri"] = TINY_HTTP_DEFAULT_URI;
}

void Config::processConfigFile(ConfigStorage &cfg)
{
	// Read config file
	// TODO
}

void Config::processEnvironment(ConfigStorage &cfg)
{
	// Read important environment variables
	// TODO
}

void Config::processArguments(ConfigStorage &cfg, ConfigStorage &args)
{
    // Merge maps with precedence to command line arguments
    ConfigStorage temp = args;
    temp.insert(cfg.begin(), cfg.end());
    cfg = temp;

	// Perform post processing.
	// TODO
    if (cfg.hasKey("help")) {
        std::cerr << getUsage();
        exit(0);
    }
}

ConfigStorage Config::parseArguments(int argc, char *argv[]) const
{
	ConfigStorage cfg;
	int i;

	// Basic argument parsing
	for (i = 1; i < argc; ++i) {
		if (argv[i][0] == '-') {
			std::string key(&argv[i][1]);
			std::string value;

			//std::cout << "key: " << key << std::endl;
			size_t equal_sign = key.find('=');
			if (equal_sign != std::string::npos) {
				value.assign(key.begin() + equal_sign + 1, key.end());
				key.resize(equal_sign);
			} else if (i + 1 < argc && argv[i + 1][0] != '-') {
				value.assign(argv[i + 1]);
				i++;
			}

			//std::cout << "key " << key << ", value " << value << std::endl;
			cfg[key] = value;
		}
	}

    return cfg;
}

void Config::processAll(int argc, char *argv[])
{
	// initial arguments parsed
	ConfigStorage cfg = parseArguments(argc, argv);
    ConfigStorage args = cfg;
	setDefaults(cfg);
	processConfigFile(cfg);
	processEnvironment(cfg);
	processArguments(cfg, args);
    *((ConfigStorage*)this) = cfg;
}

std::ostream &operator <<(std::ostream &os, const Config &config)
{
    Config::const_iterator it;
    for(it = config.begin(); it != config.end(); ++it) {
        os << it->first << ": " << it->second << std::endl;
    }

    return os;
}

} // namespace tiny_http

