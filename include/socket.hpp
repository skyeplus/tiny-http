#ifndef _TINY_HTTP_SOCKET_HPP_
#define _TINY_HTTP_SOCKET_HPP_

#include "connection.hpp"

#include <memory>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

namespace tiny_http {

class Socket
{
public:
    Socket();
    ~Socket();

    bool open();
    void close();
    bool isOpened();

    bool bind(const std::string &host, const int port);
    std::shared_ptr<Connection> waitForConnection();
    
protected:
    int sfd_;
    struct sockaddr_in addr_;
};

} // namespace tiny_http

#endif // _TINY_HTTP_SOCKET_HPP_
