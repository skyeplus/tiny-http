#ifndef _TINY_HTTP_FILE_URI_HANDLER_HPP_
#define _TINY_HTTP_FILE_URI_HANDLER_HPP_

#include "uri_handler.hpp"

#include <fstream>

namespace tiny_http {

class FileUriHandler: public UriHandler
{
public:
    FileUriHandler(const std::string &uri);
    virtual ~FileUriHandler();

    virtual std::shared_ptr<UriHandler> create(const std::string &uri);
    virtual bool isDynamic();
    virtual bool isValid();
    virtual std::istream &getStream();
    virtual std::string getContentType();
    virtual unsigned int getContentLength();

protected:
    std::ifstream file_stream_;

    std::string content_type_;
    unsigned int length_;
    std::string getFileName(Uri &uri);
    unsigned int fileSize();
};

} // namespace tiny_http

#endif // _TINY_HTTP_FILE_URI_HANDLER_HPP_
