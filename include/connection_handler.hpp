#ifndef _TINY_HTTP_CONNECTION_HANDLER_HPP_
#define _TINY_HTTP_CONNECTION_HANDLER_HPP_

#include "runnable.hpp"
#include "connection.hpp"

#include <memory>

namespace tiny_http {

class ConnectionHandler: public Runnable
{
public:
    ConnectionHandler(std::shared_ptr<Connection> conn);
    ~ConnectionHandler();

    virtual void *run();
protected:
    std::shared_ptr<Connection> conn_;
};

} // namespace tiny_http

#endif // _TINY_HTTP_CONNECTION_HANDLER_HPP_

