#ifndef _TINY_HTTP_HTTP_RESPONSE_HPP_
#define _TINY_HTTP_HTTP_RESPONSE_HPP_

#include "http_status.hpp"
#include "uri_handler.hpp"

#include <map>
#include <string>
#include <iostream>
#include <memory>

namespace tiny_http {

class HttpResponse: public std::map<std::string, std::string>
{
public:
    HttpResponse();
    ~HttpResponse();

    HttpStatus &status();
    std::string &httpVersion();
    std::string &body();
    std::string dateNow();

    bool outputBody();
    void outputBody(bool value);

    std::shared_ptr<UriHandler> getUriHandler();
    void setUriHandler(std::shared_ptr<UriHandler> handler);

protected:
    bool output_body_;
    std::string http_ver_;
    /* If handler is not valid and body is set, body will be printed */
    std::string body_;
    HttpStatus status_;
    std::shared_ptr<UriHandler> uri_handler_;
};

std::ostream &operator <<(std::ostream &os, HttpResponse &resp);

} // namespace tiny_http

#endif // _TINY_HTTP_HTTP_RESPONSE_HPP_

