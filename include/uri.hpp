#ifndef _TINY_HTTP_URI_HPP_
#define _TINY_HTTP_URI_HPP_

#include <string>

namespace tiny_http {

class Uri: public std::string
{
public:
    Uri();
    Uri(const std::string &uri);
    Uri(const char *uri);

    void parse();
    std::string path();
    std::string query();
    std::string fragment();
    std::string ext();
    bool isAuthorityPresent();

protected:
    bool parsed_;
    std::string path_;
    std::string query_;
    std::string fragment_;
    std::string ext_;
    bool authority_present_;
};

} // namespace tiny_http

#endif // _TINY_HTTP_URI_HPP_

