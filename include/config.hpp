#ifndef _TINY_HTTP_CONFIG_HPP_
#define _TINY_HTTP_CONFIG_HPP_

#include "config_storage.hpp"

namespace tiny_http {

class Config: public ConfigStorage
{
public:
	~Config();

	static Config& inst();
	void init(int argc, char *argv[]);

	std::string getUsage() const;

private:
	Config();

	void setDefaults(ConfigStorage &cfg);
	void processConfigFile(ConfigStorage &cfg);
	void processEnvironment(ConfigStorage &cfg);
	void processArguments(ConfigStorage &cfg, ConfigStorage &args);
	ConfigStorage parseArguments(int argc, char *argv[]) const;
	void processAll(int argc, char *argv[]);
};

std::ostream &operator <<(std::ostream &os, const Config &config);

} // namespace tiny_http

#endif // _TINY_HTTP_CONFIG_HPP_
