#ifndef _TINY_HTTP_LOGGER_HPP_
#define _TINY_HTTP_LOGGER_HPP_

#include "config.hpp"

#include <iostream>

namespace tiny_http {

class LoggerImpl
{
public:
	LoggerImpl();
	virtual std::ostream &getStream();
};

class Logger
{
public:

	static void init(Config &cfg);
	static std::ostream &log();
	static std::ostream &log(const char *file, const char *function, int line);

private:
	Logger();
	~Logger();

	static LoggerImpl *_logger;
};

#define LOG() Logger::log()
#define LOGF() Logger::log(__FILE__, __FUNCTION__, __LINE__)

} // namespace tiny_http

#endif // _TINY_HTTP_LOGGER_HPP_
