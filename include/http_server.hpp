#ifndef _TINY_HTTP_HTTP_SERVER_HPP_
#define _TINY_HTTP_HTTP_SERVER_HPP_

#include "thread_pool.hpp"
#include "socket.hpp"

namespace tiny_http {

class HttpServer
{
public:
    HttpServer();
    ~HttpServer();

    /* endless loop */
    void start();
    /* set internal variable to stop endless loop */
    void stop();

protected:
    ThreadPool threads_;
    Socket sock_;
    bool stop_;
};

} // namespace tiny_http

#endif // _TINY_HTTP_HTTP_SERVER_HPP_

