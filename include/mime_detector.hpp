#ifndef _TINY_HTTP_MIME_DETECTOR_HPP_
#define _TINY_HTTP_MIME_DETECTOR_HPP_

#include "uri.hpp"
#include <string>
#include <memory>

namespace tiny_http {

class MimeDetectorImpl
{
public:
    MimeDetectorImpl();
    virtual ~MimeDetectorImpl();
    virtual std::string detect(const std::string &uri) = 0;
};

class MimeDetector
{
public:
    static void init(std::shared_ptr<MimeDetectorImpl> impl);
    static std::string detect(const std::string &uri);
private:
    static std::shared_ptr<MimeDetectorImpl> mime_;
};

} // namespace tiny_http

#endif // _TINY_HTTP_MIME_DETECTOR_HPP_

