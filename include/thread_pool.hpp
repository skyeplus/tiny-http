#ifndef _TINY_HTTP_THREAD_POOL_HPP_
#define _TINY_HTTP_THREAD_POOL_HPP_

#include "common.hpp"
#include "thread.hpp"

#include <vector>

namespace tiny_http {

class ThreadPool
{
public:
    ThreadPool(unsigned int poolSize = TINY_HTTP_THREAD_POOL_SIZE);
    ~ThreadPool();

    int getPoolSize() const;
    Thread *getThread();

protected:
    std::vector<Thread*> threads_;
};

} // namespace tiny_http

#endif // _TINY_HTTP_THREAD_POOL_HPP_
