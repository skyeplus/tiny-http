#ifndef _TINY_HTTP_HTTP_HEADER_HPP_
#define _TINY_HTTP_HTTP_HEADER_HPP_

#include "connection.hpp"

#include <map>
#include <string>

namespace tiny_http {

struct CaseInsensitiveLess : public std::binary_function<std::string, std::string, bool> {
    int strcasecmp(const char *str1, const char *str2) const {
        if (!str1 && !str2)
            return 0;
        if (!str1)
            return 1;
        if (!str2)
            return -1;
        while ( *str1 && *str2 &&
                tolower((unsigned char)*str1) == tolower((unsigned char)*str2) ) {
            str1++;
            str2++;
        }
        return tolower((unsigned char)*str1) - tolower((unsigned char)*str2);
    }

    bool operator()(const std::string &s1, const std::string &s2) const {
        return strcasecmp(s1.c_str(), s2.c_str()) < 0;
    }
};

class HttpHeader: public std::map<std::string, std::string, CaseInsensitiveLess>
{
public:
    HttpHeader();
    ~HttpHeader();
    bool parse(Connection &conn);
    std::string &method();
    std::string &uri();
    std::string &httpVersion();
    std::string &body();

protected:
    bool parseInitialLine(const std::string &firstLine);
    bool parseField(const std::string &fielfValue);
    std::string method_;
    std::string uri_;
    std::string http_ver_;
    std::string body_;
};

std::ostream &operator <<(std::ostream &os, HttpHeader &header);

} // namespace tiny_http

#endif // _TINY_HTTP_HTTP_HEADER_HPP_

