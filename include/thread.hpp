#ifndef _TINI_HTTP_THREAD_HPP_
#define _TINI_HTTP_THREAD_HPP_

#include "runnable.hpp"

#include <list>
#include <memory>
#include <pthread.h>

namespace tiny_http {

class Thread {
public:
    enum ThreadState {
        NOT_STARTED = -1,
        PAUSED = 0,
        RUNNING = 1,
        COMPLETED = 2
    };

    Thread();
    virtual ~Thread();

    bool setAttr(const pthread_attr_t &attr);

    pthread_t threadId() const;

    ThreadState threadState() const;

    int getNumberOfTasks() const;

    void start(std::shared_ptr<Runnable> runn); // Add Runnable to work queue
    void *stop(); // stop this thread after runnables are finished

protected:
    // pthread data
    pthread_t thread_id_;
    pthread_attr_t thread_attr_;
    bool is_attr_set_;

    ThreadState thread_state_;
    
    // list of tasks
    std::list<std::shared_ptr<Runnable>> runnables_;
    pthread_mutex_t runn_mutex_;
    pthread_cond_t runn_available_;
    bool stop_requested_;

    bool isStopRequested() const;
    static void *threadRoutine(void *parg);
    void startThread();

    void *run();
};

} // namespace tiny_http

#endif // _TINI_HTTP_THREAD_HPP_

