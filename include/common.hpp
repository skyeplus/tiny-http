#ifndef _TINY_HTTP_COMMON_HPP_
#define _TINY_HTTP_COMMON_HPP_

namespace tiny_http {

// Global definitions
#define TINY_HTTP_CONFIG_FILE "~/.tiny_http"
#define TINY_HTTP_PUBLIC_WWW "public_www"
#define TINY_HTTP_MIME_DB_FILE "/etc/mime.types"
#define TINY_HTTP_DEFAULT_URI "/index.html"
static const int TINY_HTTP_THREAD_POOL_SIZE = 16;

} // namespace tiny_http

#endif // _TINY_HTTP_COMMON_HPP_
