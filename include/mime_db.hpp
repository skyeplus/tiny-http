#ifndef _TINY_HTTP_MIME_DB_HPP_
#define _TINY_HTTP_MIME_DB_HPP_

#include "mime_detector.hpp"

#include <map>

namespace tiny_http {

class MimeDb: public MimeDetectorImpl
{
public:
    MimeDb();

    void init(const std::string &file);
    virtual ~MimeDb();
    virtual std::string detect(const std::string &uri);
private:
    void parse(const std::string &file);
    void parseRecord(const std::string &line);
    std::map<std::string, std::string> types_;
};

} // namespace tiny_http

#endif // _TINY_HTTP_MIME_DB_HPP_

