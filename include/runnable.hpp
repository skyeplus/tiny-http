#ifndef _TINY_HTTP_RUNNABLE_HPP_
#define _TINY_HTTP_RUNNABLE_HPP_

namespace tiny_http {

class Runnable
{
public:
    virtual ~Runnable() {}
    virtual void *run() = 0;
};

} // namespace tiny_http

#endif // _TINY_HTTP_RUNNABLE_HPP_

