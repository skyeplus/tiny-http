#ifndef _TINY_HTTP_SECURITY_CHECKER_HPP_
#define _TINY_HTTP_SECURITY_CHECKER_HPP_

#include <string>

namespace tiny_http {

class SecurityChecker
{
public:
    static int checkUri(const std::string &uri);
};

} // namespace tiny_http

#endif // _TINY_HTTP_SECURITY_CHECKER_HPP_ 

