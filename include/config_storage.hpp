#ifndef _TINY_HTTP_CONFIG_STORAGE_HPP_
#define _TINY_HTTP_CONFIG_STORAGE_HPP_

#include <map>
#include <string>
#include <sstream>

namespace tiny_http {

class ConfigStorage: public std::map<std::string, std::string>
{
public:
    template <typename Type> 
    Type get(const std::string &key)
    {
    	Type result;
	    std::istringstream ss((*this)[key]);
        ss >> result;

        return result;
    }

    template <typename Type>
    void set(const std::string &key, const Type &value)
    {
        std::ostringstream ss;
        ss << value;

        (*this)[key] = ss.str();
    }

	bool hasKey(const std::string &key) const;
};

} // namespace tiny_http

#endif // _TINY_HTTP_CONFIG_STORAGE_HPP_
