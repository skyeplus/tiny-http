#ifndef _TINY_HTTP_CONNECTION_HPP_
#define _TINY_HTTP_CONNECTION_HPP_

#include <string>
#include <pthread.h>

namespace tiny_http {

static const int CONNECTION_BUF_SIZE = 4096;

class Socket;
class Connection
{
public:    
    Connection(Socket *sock, int fd);
    ~Connection();

    void close();
    bool isOpened();

    int read(char *buf, unsigned int size);
    int write(const char *buf, unsigned int size);
    std::string readline();
    
protected:
    int cfd_;
    Socket *sock_;

    int read_(char *buf, unsigned int size);
    int copyFromInternalBuf(char *buf, unsigned int size);
    void relocateToStart(unsigned int offset);
    int readToInternalBuf();

    pthread_mutex_t mutex_;
    unsigned int buf_offset_;
    char *buf_;
};

static const int IO_ATTEMPTS_NUM = 128;

} // namespace tiny_http

#endif // _TINY_HTTP_CONNECTION_HPP_
