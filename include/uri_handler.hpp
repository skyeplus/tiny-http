#ifndef _TINY_HTTP_URI_HANDLER_HPP_
#define _TINY_HTTP_URI_HANDLER_HPP_

#include "http_status.hpp"
#include "uri.hpp"

#include <iostream>
#include <string>
#include <memory>
#include <map>

namespace tiny_http {

class UriHandler
{
public:
    UriHandler(const std::string &uri);
    virtual ~UriHandler();

    virtual std::shared_ptr<UriHandler> create(const std::string &uri);
    virtual bool isDynamic();
    virtual bool isValid();
    virtual std::istream &getStream();
    virtual std::string getContentType();
    virtual unsigned int getContentLength();

    // register cgi or other dynamic content handlers here
    static void registerHandler(const std::string &ext, std::shared_ptr<UriHandler> handler);
    // return new UriHanler derived class depending on URI. Default is FileUriHandler.
    static std::shared_ptr<UriHandler> open(const std::string &uri, HttpStatus &http_status);

protected:
    Uri uri_;
private:
    static std::map<std::string, std::shared_ptr<UriHandler>> handlers_;
};

} // namespace tiny_http

#endif // _TINY_HTTP_URI_HANDLER_HPP_

