#ifndef _TINY_HTTP_HTTP_STATUS_HPP_
#define _TINY_HTTP_HTTP_STATUS_HPP_

#include <string>

namespace tiny_http {

class HttpStatus
{
public:
    HttpStatus();
    ~HttpStatus();

    void set(const int status);
    int get();
    void setUserString(const std::string &user);
    void appendString(const std::string &append);
    std::string getStatusString();
    void clear();

    static std::string defaultReason(const int status);
private:

    int status_;
    std::string user_string_;
    std::string append_string_;
};

} // namespace tiny_http

#endif // _TINY_HTTP_HTTP_STATUS_HPP_

