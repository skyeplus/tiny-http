TARGET=tiny_http

INC_DIR = include
SRC_DIR := src
OBJ_DIR := .obj
LDFLAGS = -lpthread

CXXFLAGS:=-Wall -g -I$(INC_DIR) -std=c++0x 

TARGET_BIN := bin/$(TARGET)
TARGET_SRCS := $(wildcard $(SRC_DIR)/*.cpp)
#$(warning $(TARGET_SRCS))

TARGET_OBJS := $(patsubst $(SRC_DIR)/%, $(OBJ_DIR)/%, $(TARGET_SRCS:%.cpp=%.o))

#$(warning $(TARGET_OBJS))

all: obj-dir $(TARGET_BIN)

$(TARGET_BIN): $(TARGET_OBJS) 
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

obj-dir:
	@mkdir -p $(OBJ_DIR)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/%.cpp: $(INC_DIR)/%.hpp

tests:
	make -C test

clean:
	@rm -fr $(TARGET_BIN) $(OBJ_DIR)
	@make clean -C test
